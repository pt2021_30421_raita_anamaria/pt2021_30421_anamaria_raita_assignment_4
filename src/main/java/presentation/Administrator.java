package presentation;

import bussinessLayer.DeliveryService;
import bussinessLayer.MenuItem;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static presentation.Helper.getTableModel;

public class Administrator {
    public JPanel admin;
    public JTable table1;
    private JButton insertCPButton;
    private JButton insertBButton;
    private JButton reportsButton;
    private JButton deleteButton;
    private JButton backButton;
    private JButton importButton;
    private JTextField textField1;

    public Administrator(final JFrame frame, DeliveryService deliveryService){
        deleteButton.addActionListener(e -> table1.setModel(getTableModel(deliveryService.removeBaseProduct(deliveryService.getProducts().get(table1.getSelectedRow())))));

        insertBButton.addActionListener(e -> {
            JFrame jFrame = new JFrame("Insert Base Product");
            jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            jFrame.setContentPane(new BaseProduct(deliveryService,jFrame, table1).bp);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        });

        reportsButton.addActionListener(e -> {
            JFrame jFrame = new JFrame("Reports");
            jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            jFrame.setContentPane(new Reports(deliveryService,jFrame).reports);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        });

        insertCPButton.addActionListener(e -> {
            int[] selectedIndexes = table1.getSelectedRows();
            List<MenuItem> items = new ArrayList<>(selectedIndexes.length);
            List<MenuItem> products = deliveryService.getProducts();
            for (int index : selectedIndexes) {
                items.add(products.get(index));
            }

            table1.setModel(getTableModel(deliveryService
                    .insertCompositeProduct(items, textField1.getText())));
        });

        backButton.addActionListener(e -> {
            frame.dispose();
            JFrame jFrame = new JFrame("logIn");
            try {
                jFrame.setContentPane(new Login(jFrame).login);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        });

        importButton.addActionListener(e -> table1.setModel(getTableModel(deliveryService.getProducts())));
    }


}
