package presentation;

import bussinessLayer.DeliveryService;

import javax.swing.*;

import java.time.LocalDate;

import static java.lang.Integer.parseInt;

public class Reports {
    public JPanel reports;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JButton report2Button;
    private JButton report3Button;
    private JButton report1Button;
    private JButton report4Button;

    public Reports(DeliveryService deliveryService, JFrame jFrame) {
        report1Button.addActionListener(e -> deliveryService.generateFirstReport(parseInt(textField1.getText()), parseInt(textField2.getText())));

        report2Button.addActionListener(e -> deliveryService.generateSecondReport(parseInt(textField3.getText())));

        report3Button.addActionListener(e -> deliveryService.generateThirdReport(parseInt(textField3.getText()), parseInt(textField4.getText())));

        report4Button.addActionListener(e -> {
            String[] split = textField5.getText().split("/");
            deliveryService.generateFourthReport(LocalDate.of(parseInt(split[0]), parseInt(split[1]), parseInt(split[2])), parseInt(textField3.getText()));
        });
    }
}
