package presentation;

import bussinessLayer.MenuItem;

import javax.swing.table.DefaultTableModel;
import java.util.Collection;

/**
 * This class is only used for helping me create the table of products
 * and also the method in it
 */
public class Helper {

    protected static DefaultTableModel getTableModel(Collection<MenuItem> items) {
        String col[] = {"Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        String[] data = new String[col.length];
        for (bussinessLayer.MenuItem bp : items) {
            data[0] = bp.getTitle();
            data[1] = bp.getRating() + "";
            data[2] = bp.getCalories() + "";
            data[3] = bp.getProtein() + "";
            data[4] = bp.getFat() + "";
            data[5] = bp.getSodium() + "";
            data[6] = bp.getPrice() + "";
            tableModel.addRow(data);
        }

        return tableModel;
    }
}
