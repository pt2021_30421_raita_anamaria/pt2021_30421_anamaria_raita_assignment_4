package presentation;

import bussinessLayer.DeliveryService;
import bussinessLayer.MenuItem;

import javax.swing.*;

import static presentation.Helper.getTableModel;

public class BaseProduct {
    public JTextField textField1;

    public JTextField getTextField1() {
        return textField1;
    }

    public void setTextField1(JTextField textField1) {
        this.textField1 = textField1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public void setTextField2(JTextField textField2) {
        this.textField2 = textField2;
    }

    public JTextField getTextField3() {
        return textField3;
    }

    public void setTextField3(JTextField textField3) {
        this.textField3 = textField3;
    }

    public JTextField getTextField4() {
        return textField4;
    }

    public void setTextField4(JTextField textField4) {
        this.textField4 = textField4;
    }

    public JTextField getTextField5() {
        return textField5;
    }

    public void setTextField5(JTextField textField5) {
        this.textField5 = textField5;
    }

    public JTextField getTextField6() {
        return textField6;
    }

    public void setTextField6(JTextField textField6) {
        this.textField6 = textField6;
    }

    public JTextField getTextField7() {
        return textField7;
    }

    public void setTextField7(JTextField textField7) {
        this.textField7 = textField7;
    }

    public JTextField textField2;
    public JTextField textField3;
    public JTextField textField4;
    public JTextField textField5;
    public JTextField textField6;
    public JTextField textField7;
    private JButton insertButton;
    private JButton editButton;
    public JPanel bp;
    private final JTable adminTable;

    public BaseProduct(DeliveryService deliveryService, JFrame jFrame, JTable table) {
        this.adminTable = table;

        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            MenuItem bp = deliveryService.getProducts().get(selectedRow);
            textField1.setText(bp.getTitle());
            textField1.setEditable(false);
            textField2.setText(bp.getCalories() + "");
            textField3.setText(bp.getProtein() + "");
            textField4.setText(bp.getFat() + "");
            textField5.setText(bp.getSodium() + "");
            textField6.setText(bp.getPrice() + "");
            textField7.setText(bp.getRating() + "");
        }

        insertButton.addActionListener(e -> {
            bussinessLayer.BaseProduct bp = new bussinessLayer.BaseProduct(getTextField1().getText(), Double.
                    parseDouble(getTextField7().getText()),
                    Double.parseDouble(getTextField2().getText()),
                    Double.parseDouble(getTextField4().getText()),
                    Double.parseDouble(getTextField3().getText()),
                    Double.parseDouble(getTextField5().getText()),
                    Double.parseDouble(getTextField6().getText()));
            adminTable.setModel(getTableModel(deliveryService.insertBaseProduct(bp)));
            jFrame.dispose();
        });

        editButton.addActionListener(e -> {
            bussinessLayer.BaseProduct bp = new bussinessLayer.BaseProduct(getTextField1().getText(), Double.
                    parseDouble(getTextField7().getText()),
                    Double.parseDouble(getTextField2().getText()),
                    Double.parseDouble(getTextField4().getText()),
                    Double.parseDouble(getTextField3().getText()),
                    Double.parseDouble(getTextField5().getText()),
                    Double.parseDouble(getTextField6().getText()));
            adminTable.setModel(getTableModel(deliveryService.updateBP(selectedRow, bp)));
            jFrame.dispose();
        });
    }
}
