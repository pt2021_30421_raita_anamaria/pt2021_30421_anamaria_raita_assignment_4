package presentation;

import bussinessLayer.DeliveryService;
import bussinessLayer.MenuItem;
import bussinessLayer.Order;
import dataLayer.User;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Client {
    public JPanel client;
    private JButton orderButton;
    private JButton searchButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField6;
    private JTextField textField7;
    private JButton backButton;
    private JTable table1;

    public Client(DeliveryService deliveryService, final JFrame frame, User user, Employer employer) {
        table1.setModel(Helper.getTableModel(deliveryService.getProducts()));

        searchButton.addActionListener(e -> table1.setModel(Helper.getTableModel(deliveryService.searchIn(textField1.getText(),
                textField2.getText(),
                textField3.getText(),
                textField4.getText(),
                textField5.getText(),
                textField6.getText(),
                textField7.getText()
        ))));
        orderButton.addActionListener(e -> {
            int[] selectedIndexes = table1.getSelectedRows();
            List<MenuItem> items = new ArrayList<>(selectedIndexes.length);
            List<MenuItem> products = deliveryService.getProducts();
            for (int index : selectedIndexes) {
                items.add(products.get(index));
            }
            var totalPrice = 0;
            for (MenuItem mi : items) {
                totalPrice += mi.getPrice();
            }

            deliveryService.handleOrder(user, new Order(totalPrice, user.getId(), items), employer);
        });

        backButton.addActionListener(e -> {
            frame.dispose();
            JFrame jFrame = new JFrame("logIn");
            try {
                jFrame.setContentPane(new Login(jFrame).login);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        });
    }
}
