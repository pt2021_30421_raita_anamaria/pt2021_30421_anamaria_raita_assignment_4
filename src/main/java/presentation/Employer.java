package presentation;

import bussinessLayer.Observer;

import javax.swing.*;
import java.io.IOException;

public class Employer implements Observer {
    public JPanel employer;
    private JTextArea textArea1;
    private JButton backButton;

    public Employer(final JFrame frame) {
        backButton.addActionListener(e -> {
            frame.dispose();
            JFrame jFrame = new JFrame("logIn");
            try {
                jFrame.setContentPane(new Login(jFrame).login);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        });
    }

    @Override
    public void update(Object event) {
        textArea1.append("\n" + event.toString());
    }
}
