package presentation;

import bussinessLayer.DeliveryService;
import dataLayer.Serializator;
import dataLayer.User;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Set;

public class Login {
    public JPanel login;
    private JTextField textField1;
    private JButton logInButton;
    private JButton registerButton;
    private JPasswordField passwordField1;
    private Employer employerUI;
    private final DeliveryService deliveryService;

    public static void main(String[] args) {
        JFrame frame = new JFrame("logIn");
        try {
            frame.setContentPane(new Login(frame).login);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        JOptionPane.showMessageDialog(null, "Welcome to the login screen!\n");

    }

    public Login(JFrame frame) throws IOException, ClassNotFoundException {
        deliveryService = Serializator.deserializare();

        logInButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame jFrame = new JFrame("Employer");
                jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

                if (employerUI == null) {
                    employerUI = new Employer(jFrame);
                }
                if (textField1.getText().equals("admin") && passwordField1.getText().equals("admin")) {
                    jFrame = new JFrame("Administrator");
                    jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                    jFrame.setContentPane(new Administrator(jFrame, deliveryService).admin);
                    jFrame.pack();
                    jFrame.setLocationRelativeTo(null);
                    jFrame.setVisible(true);
                } else if (textField1.getText().equals("employer") && passwordField1.getText().equals("emp")) {
                    jFrame.setContentPane(employerUI.employer);
                    jFrame.pack();
                    jFrame.setLocationRelativeTo(null);
                    jFrame.setVisible(true);
                } else {
                    try {
                        Set<User> existingUsers = Serializator.deserializareUser();
                        User user = existingUsers.stream()
                                .filter(user1 -> user1.getUsername().equals(textField1.getText()))
                                .findFirst()
                                .orElse(null);

                        if (user != null && user.getParola().equals(passwordField1.getText())) {
                            jFrame = new JFrame("Client");
                            jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                            jFrame.setContentPane(new Client(deliveryService, jFrame, user, employerUI).client);
                            jFrame.pack();
                            jFrame.setLocationRelativeTo(null);
                            jFrame.setVisible(true);
                        } else {
                            JOptionPane.showMessageDialog(null, "Please register\n");
                        }

                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    } catch (ClassNotFoundException classNotFoundException) {
                        classNotFoundException.printStackTrace();
                    }
                }


            }
        });
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Set<User> existingUsers = Serializator.deserializareUser();
                    User user = new User(textField1.getText(), passwordField1.getText());
                    existingUsers.add(user);
                    Serializator.serializareUser(existingUsers);

                } catch (IOException ioException) {
                    ioException.printStackTrace();
                } catch (ClassNotFoundException classNotFoundException) {
                    classNotFoundException.printStackTrace();
                }


            }
        });

    }
}
