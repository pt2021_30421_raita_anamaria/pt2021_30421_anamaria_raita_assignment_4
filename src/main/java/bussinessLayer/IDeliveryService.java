package bussinessLayer;

import dataLayer.User;
import presentation.Employer;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface IDeliveryService {

    /**
     * @pre bp!=null
     * @post getProducts().contains(bp);
     * @param bp
     * @return
     */
    public List<MenuItem> insertBaseProduct(BaseProduct bp);

    /**
     * @pre bp!=null
     * @param index
     * @param bp
     * @return
     */
    public List<MenuItem> updateBP(int index, BaseProduct bp);

    /**
     * @pre bps!=null;
     * @post getProducts().contains(cp);
     * @param bps
     * @param mdfTitle
     * @return
     */
    public List<MenuItem> insertCompositeProduct(List<MenuItem> bps, String mdfTitle);

    /**
     * @pre bp!=null;
     * @post !getProducts().contains(bp);
     * @param bp
     * @return
     */
    public List<MenuItem> removeBaseProduct(MenuItem bp);

    /**
     * @pre title!=null && rating!=null && calories!=null && fat!=null && protein!=null && sodium!=null && price!=null;
     * @param title
     * @param rating
     * @param calories
     * @param fat
     * @param protein
     * @param sodium
     * @param price
     * @return
     */
    public Set<MenuItem> searchIn(String title, String rating, String calories, String fat, String protein, String sodium, String price);

    /**
     * @pre order!=null;
     * @param user
     * @param order
     * @param employer
     */
    public void handleOrder(User user, Order order, Employer employer);

    /**
     * @pre startHour>=0 && endHour>=0 && startHour<=24 && endHour<=24 && startHour<endHour;
     * @param startHour
     * @param endHour
     */
    public void generateFirstReport(int startHour, int endHour);

    /**
     * @pre times>=0;
     * @param times
     */
    public void generateSecondReport(int times);

    /**
     * @pre timesForClient>=0 && amount >=0;
     * @param timesForClient
     * @param amount
     */
    public void generateThirdReport(int timesForClient, double amount);

    /**
     * @pre times>=0;
     * @param date
     * @param times
     */
    public void generateFourthReport(LocalDate date, int times);
}
