package bussinessLayer;

public interface Observer {
    void update(Object event);
}