package bussinessLayer;

import java.io.Serializable;
import java.util.Objects;

/**
 * This class is the parent class of BaseProduct and CompositeProduct
 */
public abstract class MenuItem implements Serializable, Comparable<MenuItem> {
    private String title;
    private double rating, calories, fat, protein, sodium, price;

    /**
     * This constructor builds the object CompositeProduct using the fields down below.
     *
     * @param title
     * @param rating
     * @param calories
     * @param fat
     * @param protein
     * @param sodium
     * @param price
     */
    public MenuItem(String title, double rating, double calories, double fat, double protein, double sodium, double price) {
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.fat = fat;
        this.protein = protein;
        this.sodium = sodium;
        this.price = price;
    }

    /**
     * This is a default constructor
     */
    public MenuItem() {

    }

    ;

    /**
     * This is a getter for the title of item
     *
     * @return the title of a MenuItem, a string
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method sets the title for a MenuItem
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * This is a getter for the rating of item
     *
     * @return the rating of a MenuItem, a double
     */
    public double getRating() {
        return rating;
    }

    /**
     * This method sets the rating for a MenuItem
     *
     * @param rating
     */
    public void setRating(double rating) {
        this.rating = rating;
    }

    /**
     * This is a getter for the calories of item
     *
     * @return the calories of a MenuItem, a double
     */
    public double getCalories() {
        return calories;
    }

    /**
     * This method sets the calories for a MenuItem
     *
     * @param calories
     */
    public void setCalories(double calories) {
        this.calories = calories;
    }

    /**
     * This is a getter for the fat of item
     *
     * @return the fat of a MenuItem, a double
     */
    public double getFat() {
        return fat;
    }

    /**
     * This method sets the fat for a MenuItem
     *
     * @param fat
     */
    public void setFat(double fat) {
        this.fat = fat;
    }

    /**
     * This is a getter for the protein of item
     *
     * @return the protein of MenuItem, a double
     */
    public double getProtein() {
        return protein;
    }

    /**
     * This method sets the protein for a MenuItem
     *
     * @param protein
     */
    public void setProtein(double protein) {
        this.protein = protein;
    }

    /**
     * This is a getter for the sodium of item
     *
     * @return the sodium of MenuItem, a double
     */
    public double getSodium() {
        return sodium;
    }

    /**
     * This method sets the sodium for a MenuItem
     *
     * @param sodium
     */
    public void setSodium(double sodium) {
        this.sodium = sodium;
    }

    /**
     * This is a getter for the price of item
     *
     * @return the price of MenuItem, a double
     */
    public double getPrice() {
        return price;
    }

    /**
     * This method sets the price for a MenuItem
     *
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * This method is an override of toString used to print an object
     *
     * @return a String, which represents the Menu Item attributes with their values.
     */
    @Override
    public String toString() {
        return "MenuItem{" +
                "title='" + title + '\'' +
                ", rating=" + rating +
                ", calories=" + calories +
                ", fat=" + fat +
                ", protein=" + protein +
                ", sodium=" + sodium +
                ", price=" + price +
                '}';
    }

    public abstract double totalPrice();

    /**
     * This method is used to compare 2 objects of MenuItem type using the title field
     *
     * @param o, of type MenuItem
     * @return true or false if the objects are equal and the Objects which are equal with this object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return Objects.equals(title, menuItem.title);
    }

    /**
     * @return a hash value of the sequence of input values
     */
    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public int compareTo(MenuItem o) {
        return this.getTitle().compareTo(o.getTitle());
    }
}
