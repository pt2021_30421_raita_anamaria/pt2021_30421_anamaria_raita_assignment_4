package bussinessLayer;

import java.util.ArrayList;
import java.util.List;

public class Observable {

    private final List<Observer> observers = new ArrayList<>();

    /**
     * This method is used to notify all observers when a change
     * in the state of the object is made.
     * @param event
     */
    public void changeState(Object event) {
        notifyAllObservers(event);
    }

    /**
     * This method is used to register a new observer
     * @param observer
     */
    public void register(Observer observer) {
        observers.add(observer);
    }

    /**
     * This method is used to notify all the all the observers in
     * the list of registered observers.
     * @param event
     */
    private void notifyAllObservers(Object event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }
}