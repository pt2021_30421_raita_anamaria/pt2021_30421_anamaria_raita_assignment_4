package bussinessLayer;

import java.util.List;

public class CompositeProduct extends MenuItem {
    private List<MenuItem> compositeProduct;

    /**
     * This constructor builds the object CompositeProduct using the fields down below.
     *
     * @param compositeProduct
     * @param title
     */
    public CompositeProduct(List<MenuItem> compositeProduct, String title) {
        this.compositeProduct = compositeProduct;
        this.setTitle(title);
        this.setPrice(totalPrice());
        this.setRating(totalRating());
        this.setCalories(totalCaloriess());
        this.setFat(totalFat());
        this.setProtein(totalProtein());
        this.setSodium(totalSodium());
    }

    /**
     * This method computes the total price of the menu
     *
     * @return a double value, which represents the sum of the prices of items in the menu
     */
    @Override
    public double totalPrice() {
        double price2 = 0;
        for (MenuItem bp : compositeProduct) {
            price2 += bp.getPrice();
        }
        return price2;
    }

    /**
     * This method computes the total rating of the menu
     *
     * @return a double value, which represents the sum of the ratings of items in the menu divided by the
     * nb of products
     */
    public double totalRating() {
        double rating = 0;
        for (MenuItem bp : compositeProduct) {
            rating += bp.getRating();
        }
        return rating / compositeProduct.size();
    }

    /**
     * This method computes the total calories of the menu
     *
     * @return a double value, which represents the sum of the calories of items in the menu
     */
    public double totalCaloriess() {
        double calories = 0;
        for (MenuItem bp : compositeProduct) {
            calories += bp.getCalories();
        }
        return calories;
    }

    /**
     * This method computes the total fat of the menu
     *
     * @return a double value, which represents the sum of the fat level of items in the menu
     */
    public double totalFat() {
        double fat = 0;
        for (MenuItem bp : compositeProduct) {
            fat = fat + bp.getFat();
        }
        return fat;
    }

    /**
     * This method computes the total sodium of the menu
     *
     * @return a double value, which represents the sum of the sodium quantity of items in the menu
     */
    public double totalSodium() {
        double sodium = 0;
        for (MenuItem bp : compositeProduct) {
            sodium = sodium + bp.getCalories();
        }
        return sodium;
    }

    /**
     * This method computes the total protein of the menu
     *
     * @return a double value, which represents the sum of the protein quantity of items in the menu
     */
    public double totalProtein() {
        double protein = 0;
        for (MenuItem bp : compositeProduct) {
            protein = protein + bp.getProtein();
        }
        return protein;
    }
}
