package bussinessLayer;


import dataLayer.FileWriter;
import dataLayer.Serializator;
import dataLayer.User;
import presentation.Employer;

import java.awt.*;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @invariant wellFormed();
 */
public class DeliveryService extends Observable implements IDeliveryService {
    private List<MenuItem> baseProducts;
    private static DeliveryService instance;
    private LinkedHashSet<MenuItem> cevatabeldepusaici = new LinkedHashSet<>();
    private HashMap<Order,ArrayList<MenuItem>> listtt =new HashMap<>();

    /**
     * This method is used in order to firsty read the .csv product file and
     * it collects distinct items based on the title field
     * @throws IOException
     */
    private DeliveryService() throws IOException {
        this.baseProducts = Files.lines(Paths.get("src/main/resources/products.csv"))
                .skip(1)
                .map(s -> {
                    String[] parsedLine = s.split(",");
                    return new BaseProduct(parsedLine[0],
                            Double.parseDouble(parsedLine[1]),
                            Double.parseDouble(parsedLine[2]),
                            Double.parseDouble(parsedLine[3]),
                            Double.parseDouble(parsedLine[4]),
                            Double.parseDouble(parsedLine[5]),
                            Double.parseDouble(parsedLine[6]));
                })
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * This method is used to instantiate the DeliveryService object
     * @return an instance of DeliveryService object
     * @throws IOException
     */
    public static DeliveryService getInstance() throws IOException {
        if (instance == null) {
            instance = new DeliveryService();
        }
        return instance;
    }

    /**
     * wellFormed method
     * @return the baseProducts if they are not null
     */
    public boolean wellFormed(){
        return baseProducts!=null ;
    }

    /**
     * This method is a getter for a list of MenuItems
     * @return a list of baseProducts
     */
    public List<MenuItem> getProducts() {
        return baseProducts;
    }

    /**
     * This method is used to insert a BaseProduct in the table
     * @param bp- a base product
     * @return a list of prtoducts
     */
    public List<MenuItem> insertBaseProduct(BaseProduct bp) {
        assert wellFormed();
        assert bp!=null; //preconditie!
        try {
            Set<MenuItem> menuItems = Serializator.deserializeMyMenu();
            menuItems.add(bp);
            Serializator.serializeMenuItem(menuItems);
            getProducts().addAll(menuItems);
            Collections.sort(getProducts());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        assert getProducts().contains(bp); //postconditie
        assert wellFormed();
        return getProducts();
    }

    /**
     * This is a getter for a list of type  HashMap<Order, ArrayList<MenuItem>>
     * @return a list
     */
    public HashMap<Order, ArrayList<MenuItem>> getListtt() {
        return listtt;
    }

    /**
     * This setter sets the content of the list
     * @param listtt
     */
    public void setListtt(HashMap<Order, ArrayList<MenuItem>> listtt) {
        this.listtt = listtt;
    }

    /**
     * This method is used to update a baseProduct in the table
     * @param index-of the product which is going to be updated
     * @param bp- the base product
     * @return a list of updated BaseProducts
     */

    public List<MenuItem> updateBP(int index, BaseProduct bp) {
        assert bp!=null;
        try {
            Set<MenuItem> menuItems = Serializator.deserializeMyMenu();
            if (menuItems.contains(bp)) {
                menuItems.remove(bp);
                menuItems.add(bp);
                Serializator.serializeMenuItem(menuItems);
            } else {
                getProducts().remove(index);
                getProducts().add(bp);
            }
            menuItems = Serializator.deserializeMyMenu();
            getProducts().addAll(menuItems);
            Collections.sort(getProducts());
            return getProducts();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * This method is used to insert a new composite product.
     * All its fields besides title are computed as a sum of the products
     * which compose the menu
     * @param bps- the list of baseProduct which compose the Menu
     * @param mdfTitle- the title of the ComposedMenu
     * @return a list of all products with the new one iserted
     */
    public List<MenuItem> insertCompositeProduct(List<MenuItem> bps, String mdfTitle) {
        assert bps!=null;
        try {
            CompositeProduct cp = new CompositeProduct(bps, mdfTitle);
            Set<MenuItem> menuItems = Serializator.deserializeMyMenu();
            menuItems.add(cp);
            Serializator.serializeMenuItem(menuItems);
            getProducts().addAll(menuItems);
            Collections.sort(getProducts());
            assert getProducts().contains(cp);
            return getProducts();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
    /**
     * This is a getter for an object of type  LinkedHashSet<MenuItem>
     * @return
     */
    public LinkedHashSet<MenuItem> getCevatabeldepusaici() {
        return cevatabeldepusaici;
    }

    /**
     * This setter sets the content of the LinkedHashSets
     * @param cevatabeldepusaici
     */
    public void setCevatabeldepusaici(LinkedHashSet<MenuItem> cevatabeldepusaici) {
        this.cevatabeldepusaici = cevatabeldepusaici;
    }

    /**
     * This methods removes a product from the list.
     * @param bp-a menu item
     * @return a list with the remaining products
     */
    public List<MenuItem> removeBaseProduct(MenuItem bp) {
        assert bp!=null;
        try {
            Set<MenuItem> menuItems = Serializator.deserializeMyMenu();
            if (getProducts().contains(bp)) {
                getProducts().remove(bp);

            }
            if (menuItems.contains(bp)) {
                menuItems.remove(bp);
                Serializator.serializeMenuItem(menuItems);
            }

            getProducts().removeAll(menuItems);
            getProducts().addAll(menuItems);
            Collections.sort(getProducts());
            assert !getProducts().contains(bp);
            return getProducts();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * This method is used to search for an item using as filter
     * the parameters given down below
     * @param title
     * @param rating
     * @param calories
     * @param fat
     * @param protein
     * @param sodium
     * @param price
     * @return the products found which respect the input criteria
     */
    public Set<MenuItem> searchIn(String title, String rating, String calories, String fat, String protein, String sodium, String price) {
    assert title!=null && rating!=null && calories!=null && fat!=null && protein!=null && sodium!=null && price!=null;
        try {
            Set<MenuItem> menuItems = Serializator.deserializeMyMenu();
            getProducts().addAll(menuItems);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return getProducts().stream()
                .filter(menuItem -> {
                    if (title.isBlank()) {
                        return true;
                    }
                    return menuItem.getTitle().toLowerCase().contains(title.toLowerCase());
                })
                .filter(menuItem -> {
                    if (rating.isBlank()) {
                        return true;
                    }
                    return menuItem.getRating() == Double.parseDouble(rating);
                })
                .filter(menuItem -> {
                    if (calories.isBlank()) {
                        return true;
                    }
                    return menuItem.getCalories() == Double.parseDouble(calories);
                })
                .filter(menuItem -> {
                    if (fat.isBlank()) {
                        return true;
                    }
                    return menuItem.getFat() == Double.parseDouble(fat);
                })
                .filter(menuItem -> {
                    if (protein.isBlank()) {
                        return true;
                    }
                    return menuItem.getProtein() == Double.parseDouble(protein);
                })
                .filter(menuItem -> {
                    if (sodium.isBlank()) {
                        return true;
                    }
                    return menuItem.getSodium() == Double.parseDouble(sodium);
                })
                .filter(menuItem -> {
                    if (price.isBlank()) {
                        return true;
                    }
                    return menuItem.getPrice() == Double.parseDouble(price);
                }).collect(Collectors.toCollection(LinkedHashSet::new));

    }

    /**
     * This method is used to handle an order when it is made by the client.
     * It has the user, order and employer as parameters, because when
     * an order is made, the employer must be notified.
     * @param user
     * @param order
     * @param employer
     */
    public void handleOrder(User user, Order order, Employer employer) {
        assert order!=null;
        try {
            Set<Order> orders = Serializator.deserializeOrd();
            orders.add(order);
            Serializator.serializeOrd(orders);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        register(employer);
        changeState(order);
        FileWriter.writeFile("src/main/resources/orders.txt", user.toString() + "\n" + order.toString() + "\n");
    }

    /**
     * This method is used to generate the first report
     * which print the orders from a certain hour interval
     * which has the start and end hour given as parameters.
     * @param startHour
     * @param endHour
     */
    public void generateFirstReport(int startHour, int endHour) {
        assert startHour>=0 && endHour>=0 && startHour<=24 && endHour<=24 && startHour<endHour;
        try {
            Set<Order> orders = Serializator.deserializeOrd();
            orders.stream()
                    .filter(order -> order.getDateTime().getHour() >= startHour && order.getDateTime().getHour() <= endHour)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to compute the products ordered
     * more than a specified number of times given as parameter.
     * @param times
     */
    public void generateSecondReport(int times) {
        assert times>=0;
        try {
            Set<Order> orders = Serializator.deserializeOrd();
            Map<MenuItem, Integer> productAppearances = new HashMap<>();
            orders.forEach(order -> {
                order.getProducts().forEach(menuItem -> {
                    if (productAppearances.containsKey(menuItem)) {
                        productAppearances.replace(menuItem, productAppearances.get(menuItem) + 1);
                    } else {
                        productAppearances.put(menuItem, 1);
                    }
                });
            });

            productAppearances.entrySet().stream()
                    .filter(menuItemIntegerEntry -> menuItemIntegerEntry.getValue() >= times)
                    .map(Map.Entry::getKey)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to compute the clients which order more than a number of times
     * given as parameter and also more than a certain price, also given as parameter.
     * @param timesForClient
     * @param amount
     */
    public void generateThirdReport(int timesForClient, double amount) {
        assert timesForClient>=0 && amount >=0;
        try {
            Set<Order> orders = Serializator.deserializeOrd();
            Map<User, Integer> userAppearances = new HashMap<>();
            orders.stream()
                    .filter(order -> order.getPrice() >= amount)
                    .forEach(order -> {
                        User user = getUserById(order.getClientId());
                        if (userAppearances.containsKey(user)) {
                            userAppearances.replace(user, userAppearances.get(user) + 1);
                        } else {
                            userAppearances.put(user, 1);
                        }
                    });

            userAppearances.entrySet().stream()
                    .filter(userIntegerEntry -> userIntegerEntry.getValue() >= timesForClient)
                    .map(Map.Entry::getKey)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to find a user by its id
     * @param clientId- the client id
     * @return the client with that given id
     */
    private User getUserById(String clientId) {
        Set<User> users = null;
        try {
            users = Serializator.deserializareUser();
            return users.stream()
                    .filter(user -> user.getId().equals(clientId))
                    .findFirst()
                    .orElse(null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method is used to display the  products
     * ordered  within  a  specified  day  and over the  number  of
     * times  they  have been ordered with the date and times given as parameter.
     * @param date
     * @param times
     */
    public void generateFourthReport(LocalDate date, int times) {
        assert times>=0;
        try {
            Set<Order> orders = Serializator.deserializeOrd();
            Map<User, Integer> userAppearances = new HashMap<>();
            Map<MenuItem, Integer> productAppearances = new HashMap<>();
            orders.stream()
                    .filter(order -> order.getDateTime().toLocalDate().isEqual(date))
                    .forEach(order -> {
                        order.getProducts().forEach(menuItem -> {
                            if (productAppearances.containsKey(menuItem)) {
                                productAppearances.replace(menuItem, productAppearances.get(menuItem) + 1);
                            } else {
                                productAppearances.put(menuItem, 1);
                            }
                        });
                    });

            productAppearances.entrySet().stream()
                    .filter(menuItemIntegerEntry -> menuItemIntegerEntry.getValue() >= times)
                    .map(Map.Entry::getKey)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
