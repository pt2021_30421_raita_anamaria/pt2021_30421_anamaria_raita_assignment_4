package bussinessLayer;

public class BaseProduct extends MenuItem {
    /**
     * This constructor builds the object BaseProduct using the fields down below.
     * @param title
     * @param rating
     * @param calories
     * @param fat
     * @param protein
     * @param sodium
     * @param price
     */
    public BaseProduct(String title, double rating, double calories, double fat, double protein, double sodium, double price) {
        super(title, rating, calories, fat, protein, sodium, price);
    }

    /**
     *
     * @return a double value, the price of the Object BaseProduct
     */
    public double totalPrice(){
        return getPrice();
    }
}
