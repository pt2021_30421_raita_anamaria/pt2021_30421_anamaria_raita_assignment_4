package bussinessLayer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Order implements Serializable {
    private String id;
    private String clientId;
    private double price;
    private LocalDateTime dateTime;
    private List<MenuItem> products;


    public List<MenuItem> getProducts() {
        return products;
    }

    public void setProducts(List<MenuItem> products) {
        this.products = products;
    }

    /**
     * getter for order id
     *
     * @return a string which is order id
     */
    public String getId() {
        return id;
    }

    /**
     * This method sets the order id to the id given as parameter
     *
     * @param id
     */

    public void setId(String id) {
        this.id = id;
    }

    /**
     * getter for client id
     *
     * @return a string which is client id
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * This method sets the client id to the id given as parameter
     *
     * @param clientId
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * getter for price
     *
     * @return a double which is order price
     */

    public double getPrice() {
        return price;
    }

    /**
     * This method sets the order price to the price given as parameter
     *
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * This constructor is used to obtain the Order object with price given as parameter
     *
     * @param price,clientId,products
     */
    public Order(int price, String clientId, List<MenuItem> products) {
        this.products = products;
        this.price = price;
        this.dateTime = LocalDateTime.now();
        this.clientId = clientId;
        this.id = UUID.randomUUID().toString();
    }

    /**
     * This method is used to compare 2 objects of Order type using the order and client id and date fields
     *
     * @param o, of type Order
     * @return true or false if the objects are equal and the Objects which are equal with this object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                Objects.equals(clientId, order.clientId) &&
                Objects.equals(dateTime, order.dateTime);
    }

    /**
     * @return a hash value of the sequence of input values
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, dateTime);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ",\n clientId='" + clientId + '\'' +
                ",\n price=" + price +
                ",\n data=" + dateTime +
                ",\n products=" + products +
                '}';
    }
}
