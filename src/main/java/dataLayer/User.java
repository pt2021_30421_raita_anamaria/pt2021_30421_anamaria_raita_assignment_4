package dataLayer;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class User implements Serializable {
    private String id;
    private String username;
    private String parola;
    private int orders;
    private int price;

    /**
     * This constructor is used to compute the User object using its username and password
     * @param username
     * @param parola
     */
    public User(String username, String parola) {
        this.id = UUID.randomUUID().toString();
        this.username = username;
        this.parola = parola;
        this.orders = 0;
        this.price = 0;
    }

    /**
     * This is a getter for the user id
     * @return a string which represents the user id
     */
    public String getId() {
        return id;
    }

    /**
     * This setter sets the user id to the one given as a parameter
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * This is a getter for the user username
     * @return a string which represents the username id
     */
    public String getUsername() {
        return username;
    }

    /**
     * This setter sets the user username to the one given as a parameter
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }
    /**
     * This is a getter for the user password
     * @return a string which represents the user password
     */
    public String getParola() {
        return parola;
    }

    /**
     * This setter sets the user password to the one given as a parameter
     * @param parola
     */
    public void setParola(String parola) {
        this.parola = parola;
    }
    /**
     * This is a getter for the user orders
     * @return a string which represents the user orders
     */

    public int getOrders() {
        return orders;
    }

    /**
     * This setter sets the user orders to the one given as a parameter
     * @param orders
     */
    public void setOrders(int orders) {
        this.orders = orders;
    }
    /**
     * This is a getter for the user total price of orders
     * @return a string which represents the user total price of orders
     */

    public int getPrice() {
        return price;
    }

    /**
     * This setter sets the user total price of orders to the one given as a parameter
     * @param price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ",\n username='" + username + '\'' +
                ",\n parola='" + parola + '\'' +
                ",\n orders=" + orders +
                ",\n price=" + price +
                '}';
    }

    /**
     * This method is used to compare 2 objects of User type using the username field
     * @param o, of type User
     * @return true or false if the objects are equal and the Objects which are equal with this object
     */

    /**
     *
     * @return a hash value of the sequence of input values
     */


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username) &&
                Objects.equals(parola, user.parola);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, parola);
    }
}
