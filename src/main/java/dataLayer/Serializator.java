package dataLayer;

import bussinessLayer.CompositeProduct;
import bussinessLayer.DeliveryService;
import bussinessLayer.MenuItem;
import bussinessLayer.Order;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Serializator {
    /**
     * This method is used to serialize objects of type DeliveryService
     *
     * @param deliveryService
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void serializare(DeliveryService deliveryService) throws IOException, ClassNotFoundException {
        FileOutputStream file = new FileOutputStream("src/main/resources/fisier.txt");
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(deliveryService); // Method for serialization of object
        out.close();
        file.close();

    }

    /**
     * This method is used to deserialize objects of type DeliveryService
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static DeliveryService deserializare() throws IOException, ClassNotFoundException {
        try (FileInputStream file = new FileInputStream("src/main/resources/fisier.txt");
             ObjectInputStream in = new ObjectInputStream(file)
        ) {
            return (DeliveryService) in.readObject();
        } catch (EOFException e) {
            return DeliveryService.getInstance();
        }
    }

    /**
     * This method is used to serialize a Set of Users
     *
     * @param users
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void serializareUser(Set<User> users) throws IOException, ClassNotFoundException {
        FileOutputStream file = new FileOutputStream("src/main/resources/userFile.txt");
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(users); // Method for serialization of object
        out.close();
        file.close();

    }

    /**
     * This method is used to deserialize a Set of Users
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Set<User> deserializareUser() throws IOException, ClassNotFoundException {

        try (FileInputStream file = new FileInputStream("src/main/resources/userFile.txt");
             ObjectInputStream in = new ObjectInputStream(file)
        ) {
            return (Set<User>) in.readObject();
        } catch (EOFException e) {
            return new HashSet<>();
        }

    }

    /**
     * This method is used to serialize a Set of Users
     *
     * @param myMenu
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void serializeMenuItem(Set<MenuItem> myMenu) throws IOException, ClassNotFoundException {
        FileOutputStream file = new FileOutputStream("src/main/resources/myMenu.txt");
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(myMenu); // Method for serialization of object
        out.close();
        file.close();

    }

    /**
     * This method is used to deserialize a Set of Users
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Set<MenuItem> deserializeMyMenu() throws IOException, ClassNotFoundException {

        try (FileInputStream file = new FileInputStream("src/main/resources/myCompositeMenu.txt");
             ObjectInputStream in = new ObjectInputStream(file)
        ) {
            return (Set<MenuItem>) in.readObject();
        } catch (EOFException e) {
            return new HashSet<>();
        }

    }

    /**
     * This method is used to serialize a Set of Orders
     *
     * @param cp
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void serializeOrd(Set<Order> cp) throws IOException, ClassNotFoundException {
        FileOutputStream file = new FileOutputStream("src/main/resources/serOrd.txt");
        ObjectOutputStream out = new ObjectOutputStream(file);
        out.writeObject(cp); // Method for serialization of object
        out.close();
        file.close();

    }

    /**
     * This method is used to deserialize a Set of Orders
     *
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Set<Order> deserializeOrd() throws IOException, ClassNotFoundException {

        try (FileInputStream file = new FileInputStream("src/main/resources/serOrd.txt");
             ObjectInputStream in = new ObjectInputStream(file)
        ) {
            return (Set<Order>) in.readObject();
        } catch (EOFException e) {
            return new HashSet<>();
        }

    }
}
