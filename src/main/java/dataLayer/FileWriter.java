package dataLayer;

import java.io.File;
import java.io.IOException;

/**
 * This method is used to create in a file whose path is given as a parameter
 */
public class FileWriter {
    public static void createFile(String path)
    {
        try {
            File obj = new File(path);
            obj.createNewFile();
        } catch (IOException e) {

        }
    }

    /**
     * This method is used to write a string given as a parameter in order to write in a file whose
     * path is given as a parameter.
     * @param path
     * @param c
     */
    public static void writeFile(String path,String c)
    {
        try {
            java.io.FileWriter myWriter = new java.io.FileWriter(path);
            myWriter.append(c);
            myWriter.close();
        } catch (IOException e) {
        }
    }
}
