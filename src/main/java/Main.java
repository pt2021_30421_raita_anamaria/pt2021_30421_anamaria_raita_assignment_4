import bussinessLayer.DeliveryService;
import dataLayer.Serializator;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            Serializator.serializare(DeliveryService.getInstance());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}